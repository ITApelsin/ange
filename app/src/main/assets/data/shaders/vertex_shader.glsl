#version 300 es

    struct Matrix {
        mat4 model;
        mat4 view;
        mat4 projection;
    };

    uniform Matrix mvp;

    in vec3 v_position;
//    layout(location = 1) in vec3 normal;
    in vec2 v_texture_coord;

    out vec2 texture_coord;
void main() {
    gl_Position = mvp.projection * mvp.view * vec4(v_position, 1.0);
    texture_coord = v_texture_coord;
}