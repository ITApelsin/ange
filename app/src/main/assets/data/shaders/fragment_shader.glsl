#version 300 es

    precision mediump float;

    struct Material {
        vec3 diffuse_color;
        vec3 ambient_color;
        vec3 specular_color;
        float specular_exponent;
        int flags;
    };

    const float c_zero = 0.0f;
    const float c_one = 1.0f;

    const int available_diffuse_map_flag = 0x1;

    uniform sampler2D diffuse_map;
    uniform Material material;
    in vec2 texture_coord;

    out vec4 fragmentColor;

void main() {
    fragmentColor = (material.flags & available_diffuse_map_flag) > 0 ?
                        texture(diffuse_map, texture_coord) : vec4(material.diffuse_color, c_one);
}