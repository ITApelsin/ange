//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_TEXTURE_LOADER_H

#define ANGE_TEXTURE_LOADER_H

#include "aNGE_Config.h"
#include "Texture.hpp"
#include "GLEnv.h"

class TextureLoader {
public:
    virtual ~TextureLoader(){};
    virtual Texture* loadTextureFromFile(std::string path, GLEnv *glEnv) = 0;
    
};

#endif //ANGE_TEXTURE_LOADER_H
