//
// Created by ITApelsin on 06.01.2019.
//
#include "DrawMesh.h"

aNGE::DrawMesh::DrawMesh(Mesh *mesh, MeshMaterial *material, glm::mat4 *modelMatrix) {
    this->mesh = mesh;
    this->material = material;
    this->modelMatrix = modelMatrix;
}

void aNGE::DrawMesh::execute(GLEnv *glEnv) {
    GLint materialDiffuseColor = glGetUniformLocation(glEnv->getCurentProgram(), MATERIAL_DIFFUSE_COLOR_UNIFORM);
    GLint materialAmbientColor = glGetUniformLocation(glEnv->getCurentProgram(), MATERIAL_AMBIENT_COLOR_UNIFORM);
    GLint materialSpecularColor = glGetUniformLocation(glEnv->getCurentProgram(), MATERIAL_SPECULAR_COLOR_UNIFORM);
    GLint materialSpecularExponent = glGetUniformLocation(glEnv->getCurentProgram(), MATERIAL_SPECULAR_EXPONENT_UNIFROM);
    GLint materialFlags = glGetUniformLocation(glEnv->getCurentProgram(), MATERIAL_FLAGS_UNIFORM);
    GLint diffuseMap = glGetUniformLocation(glEnv->getCurentProgram(), DIFFUSE_MAP_SAMPLER);

    glm::vec3 diffuseColor = material->diffuseColor;
    glm::vec3 ambientColor = material->ambientColor;
    glm::vec3 specularColor = material->specularColor;

    glUniform3f(materialDiffuseColor, diffuseColor.r, diffuseColor.g, diffuseColor.b);
    glUniform3f(materialAmbientColor, ambientColor.r, ambientColor.g, ambientColor.b);
    glUniform3f(materialSpecularColor, specularColor.r, specularColor.g, specularColor.b);
    glUniform1f(materialSpecularExponent, material->specularExponent);
    int flags = 0;

    if (material->diffuseMap!=nullptr) {
        flags|=1;
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, material->diffuseMap->glTextureObject);
        glUniform1i(diffuseMap, 0);
    }

    glUniform1i(materialFlags, flags);
    glBindVertexArray(mesh->vao);
    glDrawElements(GL_TRIANGLES, mesh->numIndices, GL_UNSIGNED_INT, (const void*) NULL);
}

aNGE::DrawMesh::~DrawMesh() {
    mesh = nullptr;
    material = nullptr;
    modelMatrix = nullptr;
}