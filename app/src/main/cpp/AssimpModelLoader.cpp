//
// Created by ITApelsin on 06.01.2019.
//

#include "AssimpModelLoader.h"

AssimpModelLoader::AssimpModelLoader() {
    this->importer = new Assimp::Importer();
}

AssimpModelLoader::~AssimpModelLoader() {
    delete importer;
}

Model* AssimpModelLoader::loadFromFile(const char *path, GLEnv* glEnv) {
    const aiScene* scene = importer->ReadFile( path,
                                              aiProcess_FlipWindingOrder|
                                              aiProcess_CalcTangentSpace       |
                                              aiProcess_Triangulate            |
                                              aiProcess_JoinIdenticalVertices  |
                                              aiProcess_SortByPType);
    if (!scene) {
        LOGI("Error parsing '%s': '%s'\n", path, importer->GetErrorString());
    } else {
        LOGI("aiScene import success");
    }

    GLuint vPositionLoc = static_cast<GLuint>(glGetAttribLocation(glEnv->getCurentProgram(), VERTEX_POSITION_ATTRIBUTE));
    GLuint vTextureCoordLoc = static_cast<GLuint>(glGetAttribLocation(glEnv->getCurentProgram(), VERTEX_TEXTURE_COORD_ATTRIBUTE));

    Model *model = new Model();
    model->numMaterials = scene->mNumMaterials;
    model->materials = new MeshMaterial[model->numMaterials];
    for (unsigned int i = 0; i < scene->mNumMaterials; i++) {
        aiMaterial* aiMat = scene->mMaterials[i];
        MeshMaterial material;
        aiColor4D specularColor;
        aiColor4D diffuseColor;
        aiColor4D ambientColor;
        float shininess;
        aiGetMaterialColor(aiMat, AI_MATKEY_COLOR_DIFFUSE, &diffuseColor);
        aiGetMaterialColor(aiMat, AI_MATKEY_COLOR_AMBIENT, &ambientColor);
        aiGetMaterialColor(aiMat, AI_MATKEY_COLOR_SPECULAR, &specularColor);
        aiGetMaterialFloat(aiMat, AI_MATKEY_SHININESS, &shininess);
        material.diffuseColor = glm::vec3(diffuseColor.r,
                                          diffuseColor.g,
                                          diffuseColor.b);
        material.ambientColor = glm::vec3(ambientColor.r,
                                          ambientColor.g,
                                          ambientColor.b);
        material.specularColor = glm::vec3(specularColor.r,
                                           specularColor.g,
                                           specularColor.b);
        material.specularExponent = shininess;
        material.materialName = std::string(aiMat->GetName().C_Str());
        model->materials[i] = material;
    }
    model->numMeshes = scene->mNumMeshes;
    model->meshes = new Mesh[model->numMeshes];
    LOGI("meshes num = %d", model->numMeshes);
    for (unsigned int i = 0; i < scene->mNumMeshes; i++) {
        Mesh mesh;
        const aiMesh* paiMesh = scene->mMeshes[i];
        LOGI("mesh vertices = %d", paiMesh->mNumVertices);
        float *vertexBuffValues = new float[paiMesh->mNumVertices * 5];
        for (unsigned int i = 0 ; i < paiMesh->mNumVertices ; i++) {
            const aiVector3D* pos = &(paiMesh->mVertices[i]);
            const aiVector3D* texCoord = &(paiMesh->mTextureCoords[0][i]);
            vertexBuffValues[i*5] = pos->x;
            vertexBuffValues[i*5 + 1] = pos->y;
            vertexBuffValues[i*5 + 2] = pos->z;
            vertexBuffValues[i*5 + 3] = texCoord->x;
            vertexBuffValues[i*5 + 4] = texCoord->y;
        }
        LOGI("vbo was load");
        unsigned int *indexBuffValues = new unsigned int[paiMesh->mNumFaces * 3];
        for (unsigned int i = 0 ; i < paiMesh->mNumFaces; i++) {
            const aiFace& face = paiMesh->mFaces[i];
            indexBuffValues[i*3] = face.mIndices[0];
            indexBuffValues[i*3 + 1] = face.mIndices[1];
            indexBuffValues[i*3 + 2] = face.mIndices[2];
        }
        GLuint vbo[2];
        glEnv->genBuffers(2, vbo);
        mesh.vbo = vbo[0];
        mesh.ibo= vbo[1];
        glEnv->genVertexArrays(1, &mesh.vao);
        glBindVertexArray(mesh.vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[1]);
        GLuint vbo_size = sizeof(GLfloat) * 5 * paiMesh->mNumVertices;
        __android_log_write(ANDROID_LOG_VERBOSE, "aNGE_log", "load vbo");
        glBufferData(GL_ARRAY_BUFFER, vbo_size, vertexBuffValues, GL_STATIC_DRAW);
        __android_log_write(ANDROID_LOG_VERBOSE, "aNGE_log", "load ibo");
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * paiMesh->mNumFaces * 3, indexBuffValues, GL_STATIC_DRAW);
        __android_log_write(ANDROID_LOG_VERBOSE, "aNGE_log", "buffers was loaded");

        glEnableVertexAttribArray(vPositionLoc);
        glEnableVertexAttribArray(vTextureCoordLoc);

        glVertexAttribPointer(vPositionLoc, 3, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*5, 0);
        glVertexAttribPointer(vTextureCoordLoc, 2, GL_FLOAT, GL_FALSE, sizeof(GLfloat)*5, (void*)(3 * sizeof(GLfloat)));

        glBindVertexArray(0);

        delete[] vertexBuffValues;
        delete[] indexBuffValues;

        mesh.numIndices = paiMesh->mNumFaces * 3;
        mesh.materialIndex = paiMesh->mMaterialIndex;
        model->meshes[i] = mesh;
        LOGI("mesh loaded");
    }
    LOGI("return model %s", path);
    importer->FreeScene();
    return model;
}