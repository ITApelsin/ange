//
// Created by ITApelsin on 06.01.2019.
//
#include "GLEnv.h"

GLEnv::GLEnv() {
    GLint screenParams[4];
    glGetIntegerv(GL_VIEWPORT, screenParams);
    width = screenParams[2];
    height = screenParams[3];
}

GLEnv::~GLEnv() {

    unsigned long shadersCount = shaders.size();
    unsigned long programsCount = programs.size();
    unsigned long buffersCount = buffers.size();
    unsigned long vaoCount = vao.size();
    unsigned long texturesCount = textures.size();
    unsigned long samplersCount = samplers.size();
    if (programsCount > 0) {
        for (size_t i = 0; i < programsCount; i++) {
            glDeleteProgram(programs[i]);
        }
        programs.clear();
    }
    if (shadersCount > 0) {
        for (size_t i = 0; i < shadersCount; i++) {
            glDeleteShader(shaders[i]);
        }
        shaders.clear();
    }
    if (buffersCount > 0){
        glDeleteBuffers(buffersCount, &buffers[0]);
    }
    if (vaoCount > 0) {
        glDeleteVertexArrays(vaoCount, &vao[0]);
    }
    if (texturesCount > 0) {
        glDeleteTextures(texturesCount, &textures[0]);
    }
    if (samplersCount > 0) {
        glDeleteSamplers(samplersCount, &samplers[0]);
    }
}

int GLEnv::getWidth() {
    return width;
}

int GLEnv::getHeight() {
    return height;
}

GLuint GLEnv::getCurentProgram() {
    return *curProgram;
}

GLuint GLEnv::compileShader(GLenum type, const char *src) {
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &src, NULL);
    glCompileShader(shader);
//    LOGI("compile shader\n%s", src);
    shaders.push_back(shader);
    return shaders[shaders.size() - 1];
}

GLuint GLEnv::linkProgram(GLuint vertexShader, GLuint fragmentShader, bool useThisProgram) {
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    glLinkProgram(program);
    programs.push_back(program);
    if (useThisProgram) {
        useProgram(program);
    }
    return program;
}

bool GLEnv::useProgram(GLuint program) {
    for (int i = 0; i < programs.size(); i++) {
        if (programs[i] == program) {
            glUseProgram(program);
            curProgram = &programs[i];
            return true;
        }
    }
    return false;
}

void GLEnv::genBuffers(int n, GLuint * const& buffs) {
    glGenBuffers(n, buffs);
    for (int i = 0; i < n; i++) {
        buffers.push_back(buffs[i]);
    }
}

void GLEnv::genVertexArrays(int n, GLuint * const& vertexArrays) {
    glGenVertexArrays(n, vertexArrays);
    for (int i = 0; i < n; i++) {
        vao.push_back(vertexArrays[i]);
    }
}

void GLEnv::genTextures(int n, GLuint * const& textureBuff) {
    glGenTextures(n, textureBuff);
    for (int i = 0; i < n; i++) {
        textures.push_back(textureBuff[i]);
    }
}

void GLEnv::genSamplers(int n, GLuint * const& samplersBuff) {
    glGenSamplers(n, samplersBuff);
    for (int i = 0; i < n; i++) {
        samplers.push_back(samplersBuff[i]);
    }
}