//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_CONFIG_H

#define ANGE_CONFIG_H

#define GLM_ENABLE_EXPERIMENTAL

#define SHADERS_FOLDER_PREFIX "shaders"
#define MODELS_FOLDER_PREFIX "models"
#define TEXTURES_FOLDER_PREFIX "textures"

#define DIFFUSE_MAP_POSTFIX "_diffuse.png"
#define NORMAL_MAP_POSTFIX "_normal.png"
#define SPECULAR_MAP_POSTFIX "_specular.png"

#define VERTEX_POSITION_ATTRIBUTE "v_position"
#define VERTEX_NORMAL_ATTRIBUTE "v_normal"
#define VERTEX_TEXTURE_COORD_ATTRIBUTE "v_texture_coord"

#define DIFFUSE_MAP_SAMPLER "diffuse_map"

#define MATERIAL_DIFFUSE_COLOR_UNIFORM "material.diffuse_color"
#define MATERIAL_AMBIENT_COLOR_UNIFORM "material.ambient_color"
#define MATERIAL_SPECULAR_COLOR_UNIFORM "material.specular_color"
#define MATERIAL_SPECULAR_EXPONENT_UNIFROM "material.specular_exponent"
#define MATERIAL_FLAGS_UNIFORM "material.flags"

#include <string>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <GLES3/gl3.h>
#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/ext/vector_float3.hpp>
#include <glm/vector_relational.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <android/log.h>
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "aNGE_log", __VA_ARGS__))

#endif //ANGE_CONFIG_H
