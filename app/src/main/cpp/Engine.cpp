//
// Created by ITApelsin on 06.01.2019.
//
#include "Engine.h"
namespace aNGE {

    void setPerspectiveMode(float fovy,
                            float aspect, float near, float far, GLEnv* glEnv) {
        glm::mat4 projectionMatrix = glm::perspective(fovy, aspect, near, far);
        GLint projectionMatrixLoc = glGetUniformLocation(glEnv->getCurentProgram(), "mvp.projection");
        glUniformMatrix4fv(projectionMatrixLoc, 1, GL_FALSE, glm::value_ptr(projectionMatrix));
    }

    void setCamera(float eyeX, float eyeY, float eyeZ,
                   float centerX, float centerY, float centerZ,
                   float upX, float upY, float upZ, GLEnv* glEnv) {
        glm::vec3 eye(eyeX, eyeY, eyeZ);
        glm::vec3 center(centerX, centerY, centerZ);
        glm::vec3 up(upX, upY, upZ);
        glm::mat4 viewMat = glm::lookAt(eye, center, up);
        GLint viewMatrixLoc = glGetUniformLocation(glEnv->getCurentProgram(), "mvp.view");
        glUniformMatrix4fv(viewMatrixLoc, 1, GL_FALSE, glm::value_ptr(viewMat));
    }

    void Engine::initAssetsManager(string dataFolder) {
        this->assetsManager = assetsManager = new AssetsManager(dataFolder,
                                                                     SHADERS_FOLDER_PREFIX,
                                                                     MODELS_FOLDER_PREFIX,
                                                                     TEXTURES_FOLDER_PREFIX);
    }

    void Engine::releaseAssetsManager() {
        delete assetsManager;
        assetsManager = nullptr;
    }

    Engine::Engine() {
        scene = new Scene();
        scene->objects.push_back(SceneObject("2b.obj"));
    }

    Engine& Engine::Instance() {
        static Engine instance;
        return instance;
    }

    Engine::~Engine() {
        destroyGLEnv();
        releaseAssetsManager();
        delete scene;
    }

    void Engine::initGLEnv() {
        glEnv = new GLEnv();
        glClearColor(0.5,0.5,0.5,1);
        glFrontFace(GL_CW);
        glEnable(GL_CULL_FACE);
        glEnable(GL_DEPTH_TEST);

        string vertexShaderPath = assetsManager->getAbsolutePathForResource(SHADER, "vertex_shader.glsl");
        string fragmentShaderPath = assetsManager->getAbsolutePathForResource(SHADER, "fragment_shader.glsl");

        string vertexShaderSource = assetsManager->readFileAsString(vertexShaderPath);
        string fragmentShaderSource = assetsManager->readFileAsString(fragmentShaderPath);

        GLuint vertex_shader = glEnv->compileShader(GL_VERTEX_SHADER, vertexShaderSource.c_str());
        GLuint fragment_shader = glEnv->compileShader(GL_FRAGMENT_SHADER, fragmentShaderSource.c_str());

        vertexShaderPath.clear();
        fragmentShaderPath.clear();
        vertexShaderSource.clear();
        fragmentShaderSource.clear();

        glEnv->linkProgram(vertex_shader, fragment_shader, true);
        setPerspectiveMode((float) 45, (float) glEnv->getWidth()/ glEnv->getHeight(),
                           (float) 1, (float)1000, glEnv);
    }

    void Engine::destroyGLEnv() {
        clearModelCash();
        delete glEnv;
        glEnv = nullptr;
    }

    Model* Engine::getModel(std::string name) {
        std::map<std::string, Model*>::iterator it;
        it = modelCash.find(name);
        if (it == modelCash.end()) {
            Model *model = assetsManager->importModelFromFile(name, glEnv);
            LOGI("aNGE model was imported");
            std::pair<std::string, Model*> insertPair(name, model);
            LOGI("aNGE pair was prepareted");
            std::pair<std::map<std::string,Model*>::iterator, bool> ret;
            ret = modelCash.insert(insertPair);
            if (ret.second) {
                LOGI("aNGE pair import");
            }
            return ret.first->second;
        } else {
            return it->second;
        }
    }

    std::vector<DrawStep*> Engine::buildDrawOrder() {
        std::vector<DrawStep*> drawOrder;
        for (int i = 0; i < scene->objects.size(); i++) {
            Model *model = getModel(scene->objects[i].name);
            for (int j = 0; j < model->numMeshes; j++) {
                DrawMesh *drawMeshStep = new DrawMesh(&model->meshes[j],
                                                                &model->materials[model->meshes[j].materialIndex],
                                                                &scene->objects[i].modelMatrix);
                drawOrder.push_back(drawMeshStep);
            }
        }
        return drawOrder;
    }

    void Engine::clearModelCash() {
        for (pair<std::string, Model *> e : modelCash) {
            delete e.second;
        }
        modelCash.clear();
    }

    void Engine::onDraw() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        setCamera((float)(250*sin(M_PI/180*(cameraAngle+=0.2))),100,(float) (250*cos(M_PI/180*(cameraAngle))),
                  0,70,0,0,1,0, glEnv);
        if (cameraAngle == 360) {
            cameraAngle = 0;
        }
        std::vector<DrawStep*> drawOrder = buildDrawOrder();
        for (DrawStep * e: drawOrder) {
            e->execute(glEnv);
            delete e;
        }
        drawOrder.clear();
    }
}
