//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_SCENE_H

#define ANGE_SCENE_H

#include "aNGE_Config.h"
#include "SceneObject.hpp"

class Scene {
public:
    std::vector<SceneObject> objects;
};

#endif //ANGE_SCENE_H
