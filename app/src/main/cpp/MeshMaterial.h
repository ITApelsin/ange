//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_MESH_MATERIAL_H

#define ANGE_MESH_MATERIAL_H

#include "aNGE_Config.h"
#include "Texture.hpp"

struct MeshMaterial {

    std::string materialName;

    glm::vec3 diffuseColor;
    glm::vec3 ambientColor;
    glm::vec3 specularColor;

    float specularExponent;

    Texture* normalMap = nullptr;
    Texture* diffuseMap = nullptr;
    Texture* specularMap = nullptr;
    ~MeshMaterial() {
        materialName.clear();
        delete diffuseMap;
        delete normalMap;
        delete specularMap;
        diffuseMap = nullptr;
        normalMap = nullptr;
        specularMap = nullptr;
    };
};

#endif //ANGE_MESH_MATERIAL_H
