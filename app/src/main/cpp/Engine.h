//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_H
#define ANGE_H

#include <jni.h>
#include <cmath>
#include "aNGE_Config.h"
#include "GLEnv.h"
#include "Model.hpp"
#include "AssetsManager.h"
#include "AssimpModelLoader.h"
#include "Scene.h"
#include "DrawMesh.h"


using namespace std;

namespace aNGE {

    class Engine {
    private:
        GLEnv *glEnv = nullptr;
        AssetsManager *assetsManager = nullptr;
        Scene *scene = nullptr;
        std::map<std::string, Model *> modelCash;
        float cameraAngle = 0;
    public:

        static Engine &Instance();

        Engine();

        void initAssetsManager(string dataFolder);

        void releaseAssetsManager();

        ~Engine();

        Model *getModel(std::string name);

        void clearModelCash();

        std::vector<DrawStep *> buildDrawOrder();

        void initGLEnv();

        void destroyGLEnv();

        void onDraw();
    };
}

#endif //ANGE_H
