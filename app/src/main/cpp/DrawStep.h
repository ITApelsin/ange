//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_DRAW_STEP_H
#define ANGE_DRAW_STEP_H

#include "GLEnv.h"
namespace aNGE {
    class DrawStep {
    public:
        virtual void execute(GLEnv *glEnv) = 0;

        virtual ~DrawStep() {};
    };
}

#endif //ANGE_DRAW_STEP_H
