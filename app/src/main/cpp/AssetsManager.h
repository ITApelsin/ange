//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_ASSETS_MANAGER_H

#define ANGE_ASSETS_MANAGER_H

#include "aNGE_Config.h"
#include "ModelLoader.hpp"
#include "TextureLoader.hpp"
#include "Model.hpp"
#include "PNGTextureLoader.h"
#include "AssimpModelLoader.h"
#include "GLEnv.h"

enum ResourceType {
    SHADER, MODEL, TEXTURE, NONE
};

class AssetsManager{
private:
    std::string dataFolder;
    std::string modelFolderPrefix;
    std::string textureFolderPrefix;
    std::string shaderFolderPrefix;
    ModelLoader* modelLoader;
    TextureLoader* textureLoader;

public:
    AssetsManager(std::string dataFolder,
                        std::string shaderFolderPrefix,
                        std::string modelFolderPrefix,
                        std::string textureFolderPrefix);
    ~AssetsManager();
    std::string readFileAsString(std::string src);
    std::string getAbsolutePathForResource(ResourceType type, std::string src);
    Model* importModelFromFile(std::string modelName, GLEnv* glEnv);
};


#endif //ANGE_ASSETS_MANAGER_H
