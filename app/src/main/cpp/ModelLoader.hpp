//
// Created by ITApelsin on 06.01.2019.
//
#ifndef ANGE_MODEL_LOADER_H

#define ANGE_MODEL_LOADER_H

#include "aNGE_Config.h"
#include "Model.hpp"
#include "MeshMaterial.h"
#include "GLEnv.h"

class ModelLoader {
public:
    virtual ~ModelLoader(){};
    virtual Model* loadFromFile(const char *path, GLEnv* glEnv) = 0;
};

#endif //ANGE_MODEL_LOADER_H