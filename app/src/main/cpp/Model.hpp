//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_MODEL_H

#define ANGE_MODEL_H
#include "aNGE_Config.h"
#include "MeshMaterial.h"
#include "Mesh.h"
#include "GLEnv.h"

class Model {
public:
    MeshMaterial* materials;
    Mesh* meshes;
    unsigned int numMeshes;
    unsigned int numMaterials;
    ~Model() {
        LOGI("destroy model");
        delete[] meshes;
        delete[] materials;
    };
};

#endif //ANGE_MODEL_H
