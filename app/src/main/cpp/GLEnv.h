//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_GLENV_H

#define ANGE_GLENV_H

#include "aNGE_Config.h"

class GLEnv {
private:
    std::vector<GLuint> shaders;
    std::vector<GLuint> programs;
    std::vector<GLuint> buffers;
    std::vector<GLuint> vao;
    std::vector<GLuint> samplers;
    std::vector<GLuint> textures;
    int width, height;
    GLuint *curProgram;
public:
    GLEnv();
    ~GLEnv();
    int getWidth();
    int getHeight();
    GLuint getCurentProgram();
    GLuint compileShader(GLenum type, const char * src);
    GLuint linkProgram(GLuint vertexShader, GLuint fragmentShader, bool useThisProgram);
    bool useProgram(GLuint program);
    void genBuffers(int n, GLuint * const & buffers);
    void genVertexArrays(int n, GLuint * const & vao);
    void genTextures(int n, GLuint * const & textures);
    void genSamplers(int n, GLuint * const& samplers);
};

#endif //ANGE_GLENV_H
