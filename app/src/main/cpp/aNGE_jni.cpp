//
// Created by ITApelsin on 06.01.2019.
//
#include <jni.h>
#include "Engine.h"
using namespace std;

void onCreate(string dataFolder) {
    aNGE::Engine& instance = aNGE::Engine::Instance();
    instance.initAssetsManager(dataFolder);
    instance.initGLEnv();
}

void onResize(int width, int height) {
    aNGE::Engine& instance = aNGE::Engine::Instance();
    instance.destroyGLEnv();
    instance.initGLEnv();
}

void onDraw() {
    aNGE::Engine& instance = aNGE::Engine::Instance();
    instance.onDraw();
}

extern "C"
JNIEXPORT void JNICALL
Java_ru_itapelsin_ange_ANGEView_nativeOnCreate(JNIEnv *env, jclass type, jstring dataFolder_) {
    const char *dataFolder = env->GetStringUTFChars(dataFolder_, 0);
    onCreate(string(dataFolder));
    env->ReleaseStringUTFChars(dataFolder_, dataFolder);
}

extern "C"
JNIEXPORT void JNICALL
Java_ru_itapelsin_ange_ANGEView_nativeOnResize(JNIEnv *env, jclass type, jint width, jint height) {
    onResize(width, height);
}

extern "C"
JNIEXPORT void JNICALL
Java_ru_itapelsin_ange_ANGEView_nativeOnDraw(JNIEnv *env, jclass type) {
    onDraw();
}
