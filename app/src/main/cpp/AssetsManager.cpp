//
// Created by ITApelsin on 06.01.2019.
//
#include "AssetsManager.h"

AssetsManager::AssetsManager(std::string dataFolder,
                                       std::string shaderFolderPrefix,
                                       std::string modelFolderPrefix,
                                       std::string textureFolderPrefix) {
    this->dataFolder = dataFolder;
    this->shaderFolderPrefix = shaderFolderPrefix;
    this->modelFolderPrefix = modelFolderPrefix;
    this->textureFolderPrefix = textureFolderPrefix;
    this->textureLoader = new PNGTextureLoader();
    this->modelLoader = new AssimpModelLoader();

}

AssetsManager::~AssetsManager() {
    delete textureLoader;
    delete modelLoader;
}

std::string AssetsManager::getAbsolutePathForResource(ResourceType type, std::string src) {
    switch (type) {
        case SHADER:
            return dataFolder + "/" + shaderFolderPrefix + "/" + src;
        case TEXTURE:
            return dataFolder + "/" + textureFolderPrefix + "/" + src;
        case MODEL:
            return dataFolder + "/" + modelFolderPrefix + "/" + src;
        case NONE:
            return dataFolder + "/" + src;
    }
    return nullptr;
}


std::string AssetsManager::readFileAsString(std::string path) {
    std::fstream file(path);
    std::string content;
    if (file.is_open()) {
        std::string line;
        while (getline(file, line)) {
            content += line + "\n";
        }
        path.clear();
        file.close();
    }
    return content;
}

Model* AssetsManager::importModelFromFile(std::string modelName, GLEnv * glEnv) {
    std::string modelPath = getAbsolutePathForResource(MODEL, modelName);
    Model *model = modelLoader->loadFromFile(modelPath.c_str(), glEnv);
    for (int i = 0; i < model->numMaterials; i++) {
        std::string diffuseMapPath = getAbsolutePathForResource(TEXTURE, model->materials[i].materialName + DIFFUSE_MAP_POSTFIX);
        std::string normalMapPath = getAbsolutePathForResource(TEXTURE, model->materials[i].materialName + NORMAL_MAP_POSTFIX);
        std::string specularMapPath = getAbsolutePathForResource(TEXTURE, model->materials[i].materialName + SPECULAR_MAP_POSTFIX);

        model->materials[i].diffuseMap = textureLoader->loadTextureFromFile(diffuseMapPath, glEnv);
        model->materials[i].normalMap = textureLoader->loadTextureFromFile(normalMapPath, glEnv);
        model->materials[i].specularMap = textureLoader->loadTextureFromFile(specularMapPath, glEnv);
    }
    return model;
}