//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_MESH_H

#define ANGE_MESH_H
#include "aNGE_Config.h"
#include "GLEnv.h"

struct Mesh {
    GLuint vbo;
    GLuint ibo;
    GLuint vao;
    unsigned int numIndices;
    unsigned int numVertex;
    int materialIndex;
};

#endif //ANGE_MESH_H
