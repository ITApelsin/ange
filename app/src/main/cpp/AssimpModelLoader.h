//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_ASSIMP_MODEL_LOADER_H

#define ANGE_ASSIMP_MODEL_LOADER_H

#include "ModelLoader.hpp"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

class AssimpModelLoader : public ModelLoader{
private:
    Assimp::Importer *importer;
public:

    AssimpModelLoader();
    ~AssimpModelLoader();
    Model* loadFromFile(const char *path, GLEnv* glEnv);
};


#endif //ANGE_ASSIMP_MODEL_LOADER_H
