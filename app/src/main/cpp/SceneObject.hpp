//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_SCENE_OBJECT_H

#define ANGE_SCENE_OBJECT_H

#include "aNGE_Config.h"

struct SceneObject {
public:
    std::string name;
    glm::mat4 modelMatrix;
    SceneObject(std::string objectRefName) {
        name = objectRefName;
        modelMatrix = glm::mat4(1.0f);
    }
};
#endif //ANGE_SCENE_OBJECT_H
