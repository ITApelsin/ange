//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_PNG_TEXTURE_LOADER_H

#define ANGE_PNG_TEXTURE_LOADER_H

#include "TextureLoader.hpp"
#include "GLEnv.h"
#include "png.h"

class PNGTextureLoader : public TextureLoader {

public:
    Texture* loadTextureFromFile(std::string path, GLEnv* glEnv);
};


#endif //ANGE_PNG_TEXTURE_LOADER_H
