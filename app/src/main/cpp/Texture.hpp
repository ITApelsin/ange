//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_TEXTURE_H

#define ANGE_TEXTURE_H

#include "aNGE_Config.h"

struct Texture {
    std::string src;
    GLuint glTextureObject;
    unsigned int width;
    unsigned int height;
    int bit_depth;
    int color_type;
};

#endif //ANGE_TEXTURE_H
