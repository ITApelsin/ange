//
// Created by ITApelsin on 06.01.2019.
//

#ifndef ANGE_DRAW_MESH_H
#define ANGE_DRAW_MESH_H

#include "DrawStep.h"
#include "Mesh.h"
#include "MeshMaterial.h"
namespace aNGE {
    class DrawMesh : public DrawStep {
    private:
        Mesh *mesh;
        MeshMaterial *material;
        glm::mat4 *modelMatrix;
    public:
        DrawMesh(Mesh *mesh,
                 MeshMaterial *material,
                 glm::mat4 *modelMatrix);

        void execute(GLEnv *glEnv);

        ~DrawMesh();
    };
}

#endif //ANGE_DRAW_MESH_H
