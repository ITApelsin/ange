//
// Created by ITApelsin on 06.01.2019.
//
#include "PNGTextureLoader.h"

Texture* PNGTextureLoader::loadTextureFromFile(std::string path, GLEnv* glEnv) {
    FILE * file = fopen(path.c_str(), "rb");
    if (file) {
        png_uint_32 imWidth, imHeight;
        Texture* texture = new Texture();
        int colorType;
        fseek(file, 8, SEEK_CUR);
        png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        png_infop info_ptr = png_create_info_struct(png_ptr);
        png_init_io(png_ptr, file);
        png_set_sig_bytes(png_ptr, 8);
        png_read_info(png_ptr, info_ptr);
        png_get_IHDR(png_ptr, info_ptr, &imWidth, &imHeight, &texture->bit_depth, &colorType, NULL, NULL, NULL);
        texture->height = static_cast<unsigned int>(imHeight);
        texture->width = static_cast<unsigned int>(imWidth);
        int row = texture->width * (colorType == PNG_COLOR_TYPE_RGBA ? 4 : 3);
        texture->color_type = colorType == PNG_COLOR_TYPE_RGBA ? GL_RGBA : GL_RGB;
        unsigned char *data = new unsigned char[row * texture->height];
        png_bytep * row_pointers = new png_bytep[texture->height];
        for(int i = 0; i < texture->height; ++i)
            row_pointers[i] = (png_bytep) (data + i * row);
        png_read_image(png_ptr, row_pointers);
        png_destroy_read_struct(&png_ptr, &info_ptr, 0);
        delete[] row_pointers;
        glGenTextures(1, &texture->glTextureObject);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture->glTextureObject);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        if (colorType == PNG_COLOR_TYPE_RGBA) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->width, texture->height,
                         0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        }else {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->width, texture->height,
                         0, GL_RGB, GL_UNSIGNED_BYTE, data);
        }
        glBindTexture(GL_TEXTURE_2D, 0);
        delete[] data;
        return texture;
    } else {
        return nullptr;
    }
}