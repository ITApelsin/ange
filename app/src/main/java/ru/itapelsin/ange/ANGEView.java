package ru.itapelsin.ange;

import android.content.Context;
import android.opengl.GLSurfaceView;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

public class ANGEView extends GLSurfaceView {

    static {
        System.loadLibrary("aNGE");
    }

    public ANGEView(Context context) {
        super(context);
        setEGLContextClientVersion(3);
        setRenderer(new GLRenderer());
    }

    public static native void nativeOnCreate(String dataFolder);
    public static native void nativeOnResize(int width, int height);
    public static native void nativeOnDraw();

    private class GLRenderer implements GLSurfaceView.Renderer {

        @Override
        public void onSurfaceCreated(GL10 gl, EGLConfig config) {
            nativeOnCreate(getContext().getCacheDir().getAbsolutePath() + "/data");
        }

        @Override
        public void onSurfaceChanged(GL10 gl, int width, int height) {
            nativeOnResize(width, height);
        }

        @Override
        public void onDrawFrame(GL10 gl) {
            nativeOnDraw();
        }
    }
}
