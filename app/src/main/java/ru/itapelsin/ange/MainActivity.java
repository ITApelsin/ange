package ru.itapelsin.ange;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import java.io.IOException;

import ru.itapelsin.ange.utils.AssetsUtils;
import ru.itapelsin.ange.utils.OldStyleAssetsUtils;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getSupportActionBar().hide();
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        try {
            AssetsUtils utils = new OldStyleAssetsUtils();
            utils.unzipAssetsFolder(this.getAssets(), "data",  this.getCacheDir());
        } catch (IOException e) {
            e.printStackTrace();
            finish();
        }
        setContentView(new ANGEView(this));
    }

}
