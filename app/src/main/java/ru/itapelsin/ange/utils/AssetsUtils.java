package ru.itapelsin.ange.utils;

import android.annotation.SuppressLint;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public abstract class AssetsUtils {

    @SuppressLint("Assert")
    public void unzipAssetsFolder(AssetManager manager, String srcFileName, File destFolder) throws IOException {
        assert destFolder.exists() : "Folder is not exist";

        String[] files = manager.list(srcFileName);
        if (files.length != 0){
            Log.d("AssetsUtils","it is folder: " + Arrays.toString(files));
            for (String file : files) {
                File filesFolder = new File(destFolder, srcFileName);
                if (!filesFolder.exists() && !filesFolder.mkdir()) {
                    throw new IOException("can not create directory: " + filesFolder.getAbsolutePath());
                }
                unzipAssetsFolder(manager, srcFileName+"/"+file, destFolder);
            }
        } else {
            writeFile(manager.open(srcFileName), new File(destFolder, srcFileName));
        }
    }

    protected abstract void writeFile(InputStream is, File path) throws IOException;

}