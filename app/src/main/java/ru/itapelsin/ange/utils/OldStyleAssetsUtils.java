package ru.itapelsin.ange.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class OldStyleAssetsUtils extends AssetsUtils {

    @Override
    protected void writeFile(InputStream is, File path) throws IOException {
        int numReads = 0;
        byte[] buffer = new byte[1000 * 1024];
        FileOutputStream fos = null;
        try {
          fos = new FileOutputStream(path);
          while ((numReads = is.read(buffer))!=-1) {
              fos.write(buffer, 0, numReads);
          }
          fos.flush();
        } finally {
            if (fos!=null) {
                fos.close();
            }
        }
    }

}
